const books = [
   {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70
   },
   {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
   },
   {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
   },
   {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
   },
   {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40
   },
   {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
   }
];

const requiredProps = ['author', 'name', 'price'];


function checkPropInObj(obj, requiredProps, num = 0) {
   requiredProps.forEach((prop) => {
      if (!obj.hasOwnProperty(prop)) {
         throw new SyntaxError(`Відсутня властивість ${prop} у об'єкта №${num + 1}`)
      }
   })
};

function crateUlString(obj) {
   let listString = '';
   for (const [key, value] of Object.entries(obj)) {
      listString += `<li><strong>${key}</strong> - ${value},</li>`;
   }
   return `<ul style='text-align: left; padding: 10px;'>${listString}</ul>`;
}

function addListOnPage(arr, requiredProps) {
   arr.forEach((obj, i) => {
      try {
         checkPropInObj(obj, requiredProps, i);
         let ulString = crateUlString(arr[i]);
         document.querySelector('#root').insertAdjacentHTML('beforeend', `${ulString}`);
      } catch (error) {
         console.log(error);
      }
   })
};
addListOnPage(books, requiredProps);
